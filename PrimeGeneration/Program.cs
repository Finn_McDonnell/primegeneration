﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PrimeGeneration
{
    class Program
    {
        static int limit;
        static StreamReader instream = new StreamReader(Console.OpenStandardInput());
        static HashSet<long> workingList;
        static HashSet<long> FinalList;

        static void Main(string[] args)
        {
            workingList = new HashSet<long>();
            FinalList = new HashSet<long>();

            Console.WriteLine("What is your limit for finding primes?");
            limit = chooseNonNegativeNumber();

            fillList(limit); //Populate the working list to be refined

            refineList();
            
            printList(FinalList); //Output solution list

        }


        static int chooseNonNegativeNumber() //Grab user input and prompt them again if it is not valid.
        {
            int testNumber = chooseNumber();
            if (testNumber < 0)
            {
                Console.Write("Must be non negative.\n");
                return chooseNonNegativeNumber();
            }
            else return testNumber;
        }

        static int chooseNumber() //Read in line, validate the input and prompt user again if there are problems.
        {
            Console.WriteLine("Enter a non negative integer:\n");
            string line = instream.ReadLine().Trim();
            int requestedNumber;

            try
            {
                requestedNumber = Convert.ToInt32(line);
                return requestedNumber;
            }
            catch (OverflowException)
            {
                Console.Write("Number is too large.\n");
                return chooseNumber();
            }
            catch (FormatException)
            {
                Console.Write("Not a number.\n");
                return chooseNumber();
            }
        }

        static void fillList(int upperLimit) //Populate an initial number set with natural numbers
        {
            for (int i = 2; i <= upperLimit; i++) workingList.Add(i);
        }

        static void refineList() //Refine working list
        {
            for (int i = 2; i <= limit; i++) //Loop through natural numbers
            {
                if (workingList.Contains((long) i)) //Check if they have been removed
                {
                    refineForGivenSeed((long) i); //If not then remove multiples
                    FinalList.Add((long) i); //Add them to list of primes for solution
                    workingList.Remove((long) i); //Remove them from the working list so they cannot be re processed
                }

            }
        }

        static void refineForGivenSeed(long seed)
        {
            for (long j = seed * seed; j <= limit; j += seed) //Loop from seed squared as multiples of seed below this have already been eliminated, through multiples of seed
            {
                if (workingList.Contains(j))
                {
                    workingList.Remove(j); //Remove the multiple of seed if it has not already been removed
                }
            }
        }


        static void printList(HashSet<long> list) //Output contents of a number set
        {
            foreach (int j in list) Console.WriteLine(j);
        }
    }
}
